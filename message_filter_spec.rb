require './message_filter'

describe MessageFilter do
  describe '#detect?', 'with argument foo' do
    subject { @filter = MessageFilter.new('foo') }
    # RSpec3.0 is_expected.to, is_expected.not_to
    it { should be_detect('hello from foo') }
    it { should_not be_detect('hello tdd world!') }
  end

  describe '#detect?', 'with argument foo, bar' do
    subject { @filter = MessageFilter.new('foo', 'bar') }
    it { should be_detect('hello from bar') }
    it { should be_detect('hello from foo') }
    it { should_not be_detect('hello tdd world!') }
  end
end
